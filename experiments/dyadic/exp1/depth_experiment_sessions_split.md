# Dyadic Exp1: Studying depth of networks using session based splits
## Info

The following experiment is conducted to study the affect of dyadic
network depth. The data set used is split into training, validation and
testing using sessions.


<a id="org948f4f8"></a>

## Architecture

-   One dyad

![img](images/dyad.png)

-   Kernel size: 3x3x3
-   Number of kernels: [4, 8, 16, 32]
-   MaxPool3D: 3x3x3 with stride 3.
-   Max depth: 4


<a id="org3e98ae8"></a>

## System info

-   CPU: Intel(R) Xeon(R) Silver 4208 CPU @ 2.10GHz
-   GPU: Quadro RTX 5000
    -   VRAM: 16 GB
    -   Tensor Cores: 384
    -   CUDA Parallel Processing cores: 3072
-   Memory: 128 GB


<a id="orga9d1723"></a>

## Dataset

-   [Trimmed video dataset with one trim per instance (resized to 224)](../../../datasets/one_video_per_instance_224/split_using_sessions.md)
-   Splits are based on sessions
-   typing/notyping:
    -   Training: 142 samples per instance
    -   Validation: all
-   writing/nowriting:
    -   Training: 150 samples per instance
    -   Validation: all
-   Data processing:
    -   Augmentation: None
    -   Resizing: 224x224


<a id="orgb8df8e7"></a>

## Training parameters:

-   Learning rate: 0.001
-   Momentum: 0.9
-   Max Epochs: 50
-   **Best model:** A model that has least gap between training and validation accuracies is considered the best.
    If two epochs produce same difference then the one having higher accuracy is considered to be
    the best.


<a id="org827cf1f"></a>

## Results

The training and validation accuracies below are captured for the **best model**.


<a id="org423b7af"></a>

### typing/notyping

![img](../imgs/experiments/tynty_dyadic_nn_depth_experiments.png "Accuracy vs epoch [**Interactive link**](./tynty_dyadic_nn_depth_experiments.html)")


<a id="orgbdce6ba"></a>

#### Best = Least accuracy gap

Best implies minimum gap between training and validation accuracies
If two epochs have same gap then the one having higher validation accuracy
is considered to be the best.

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
<caption class="t-above"><span class="table-number">Table 1:</span> Performace of dyadic networks.</caption>

<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">Dyads</th>
<th scope="col" class="org-left"># parameters</th>
<th scope="col" class="org-right">best epoch</th>
<th scope="col" class="org-right">trn acc</th>
<th scope="col" class="org-right">val acc</th>
</tr>


<tr>
<th scope="col" class="org-right">&#xa0;</th>
<th scope="col" class="org-left">&#xa0;</th>
<th scope="col" class="org-right">&#xa0;</th>
<th scope="col" class="org-right">&#xa0;</th>
<th scope="col" class="org-right">&#xa0;</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">1</td>
<td class="org-left">657,457</td>
<td class="org-right">1</td>
<td class="org-right">0.57</td>
<td class="org-right">0.57</td>
</tr>


<tr>
<td class="org-right">2</td>
<td class="org-left">47,305</td>
<td class="org-right">1</td>
<td class="org-right">0.73</td>
<td class="org-right">0.73</td>
</tr>


<tr>
<td class="org-right">3</td>
<td class="org-left">7,801</td>
<td class="org-right">0</td>
<td class="org-right">0.59</td>
<td class="org-right">0.61</td>
</tr>


<tr>
<td class="org-right">4</td>
<td class="org-left">18,777</td>
<td class="org-right">1</td>
<td class="org-right">0.73</td>
<td class="org-right">0.73</td>
</tr>
</tbody>
</table>


<a id="orge30cc40"></a>

#### Best = Highest validation accuracy

Best implies highest validation accuracy

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
<caption class="t-above"><span class="table-number">Table 2:</span> Performace of dyadic networks.</caption>

<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">Dyads</th>
<th scope="col" class="org-left"># parameters</th>
<th scope="col" class="org-right">best epoch</th>
<th scope="col" class="org-right">trn acc</th>
<th scope="col" class="org-right">val acc</th>
</tr>


<tr>
<th scope="col" class="org-right">&#xa0;</th>
<th scope="col" class="org-left">&#xa0;</th>
<th scope="col" class="org-right">&#xa0;</th>
<th scope="col" class="org-right">&#xa0;</th>
<th scope="col" class="org-right">&#xa0;</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">1</td>
<td class="org-left">657,457</td>
<td class="org-right">1</td>
<td class="org-right">0.57</td>
<td class="org-right">0.57</td>
</tr>


<tr>
<td class="org-right">2</td>
<td class="org-left">47,305</td>
<td class="org-right">18</td>
<td class="org-right">0.99</td>
<td class="org-right">0.77</td>
</tr>


<tr>
<td class="org-right">3</td>
<td class="org-left">7,801</td>
<td class="org-right">6</td>
<td class="org-right">0.91</td>
<td class="org-right">0.82</td>
</tr>


<tr>
<td class="org-right">4</td>
<td class="org-left">18,777</td>
<td class="org-right">19</td>
<td class="org-right">0.91</td>
<td class="org-right">0.85</td>
</tr>
</tbody>
</table>


<a id="org2ec9432"></a>

### writing/nowriting

![img](../imgs/experiments/wnw_dyadic_nn_depth_experiments.png "Accuracy vs epoch [**Interactive link**](./wnw_dyadic_nn_depth_experiments.html)")


<a id="org02a2499"></a>

#### Best = Least accuracy gap

Best implies minimum gap between training and validation accuracies
If two epochs have same gap then the one having higher validation accuracy
is considered to be the best.

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
<caption class="t-above"><span class="table-number">Table 3:</span> Performace of dyadic networks.</caption>

<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">Dyads</th>
<th scope="col" class="org-left"># parameters</th>
<th scope="col" class="org-right">best epoch</th>
<th scope="col" class="org-right">trn acc</th>
<th scope="col" class="org-right">val acc</th>
</tr>


<tr>
<th scope="col" class="org-right">&#xa0;</th>
<th scope="col" class="org-left">&#xa0;</th>
<th scope="col" class="org-right">&#xa0;</th>
<th scope="col" class="org-right">&#xa0;</th>
<th scope="col" class="org-right">&#xa0;</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">1</td>
<td class="org-left">657,457</td>
<td class="org-right">4</td>
<td class="org-right">0.60</td>
<td class="org-right">0.61</td>
</tr>


<tr>
<td class="org-right">2</td>
<td class="org-left">47,305</td>
<td class="org-right">0</td>
<td class="org-right">0.64</td>
<td class="org-right">0.51</td>
</tr>


<tr>
<td class="org-right">3</td>
<td class="org-left">7,801</td>
<td class="org-right">6</td>
<td class="org-right">0.86</td>
<td class="org-right">0.77</td>
</tr>


<tr>
<td class="org-right">4</td>
<td class="org-left">18,777</td>
<td class="org-right">4</td>
<td class="org-right">0.81</td>
<td class="org-right">0.80</td>
</tr>
</tbody>
</table>


<a id="orge7e6496"></a>

#### Best = Highest validation accuracy

Best implies highest validation accuracy

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
<caption class="t-above"><span class="table-number">Table 4:</span> Performace of dyadic networks.</caption>

<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">Dyads</th>
<th scope="col" class="org-left"># parameters</th>
<th scope="col" class="org-right">best epoch</th>
<th scope="col" class="org-right">trn acc</th>
<th scope="col" class="org-right">val acc</th>
</tr>


<tr>
<th scope="col" class="org-right">&#xa0;</th>
<th scope="col" class="org-left">&#xa0;</th>
<th scope="col" class="org-right">&#xa0;</th>
<th scope="col" class="org-right">&#xa0;</th>
<th scope="col" class="org-right">&#xa0;</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">1</td>
<td class="org-left">657,457</td>
<td class="org-right">2</td>
<td class="org-right">0.68</td>
<td class="org-right">0.62</td>
</tr>


<tr>
<td class="org-right">2</td>
<td class="org-left">47,305</td>
<td class="org-right">21</td>
<td class="org-right">1.00</td>
<td class="org-right">0.75</td>
</tr>


<tr>
<td class="org-right">3</td>
<td class="org-left">7,801</td>
<td class="org-right">19</td>
<td class="org-right">0.97</td>
<td class="org-right">0.80</td>
</tr>


<tr>
<td class="org-right">4</td>
<td class="org-left">18,777</td>
<td class="org-right">9</td>
<td class="org-right">0.85</td>
<td class="org-right">0.82</td>
</tr>
</tbody>
</table>


<a id="org4580f2a"></a>

## Inference

-   Try goup leave one out (exp2)
-   Do not consider one dyad(dyad<sub>1</sub>) for future experiments.

