# Proj Exp1: Hand detection projections(full video)

## Introduction

Here we explore detecting active regions by projecting hand detection
bounding boxes for entire video.


## System info
-   CPU: Intel(R) Xeon(R) Silver 4208 CPU @ 2.10GHz
-   GPU: Quadro RTX 5000
    -   VRAM: 16 GB
    -   Tensor Cores: 384
    -   CUDA Parallel Processing cores: 3072
-   Memory: 128 GB
## Hand detection method
For more information please contact *Sravani Teeprthi*.
-   Algorithm: Single shot multibox detector
-   Github link: <https://github.com/victordibia/handtracking>
-   Training data: We retrained using AOLME images
-   Framework: Tensorflow 2
-   Detection rate: We ran detection every one second.
## Testing video:
We tested generating projection maps and thresholding them at 90 percentile
on `C2L1P-B-Feb23`[(AOLME link <&#x2013; Login required)](https://aolme.unm.edu/Videos/cur_group_videos.php?cohort=2&school=Polk&level=1&group=B&date=2018-02-23)
## Results
Here we present projection maps(in gray scale) and also active regions when
thresholding at 90 percentile. It can be clearly seen that once the group
get settled we are able to detect active regions clearly.
### `G-C2L1P-Feb23-B-Shelby_q2_01-06`

![img](./imgs/Proj_01.png "Projection map")

![img](./imgs/Proj01_with_threshold.png "Projection map threshold at 90 percentile")



### `G-C2L1P-Feb23-B-Shelby_q2_02-06`

![img](./imgs/Proj_02.png "Projection map")

![img](./imgs/Proj02_with_threshold.png "Projection map threshold at 90 percentile")



### `G-C2L1P-Feb23-B-Shelby_q2_03-06`

![img](./imgs/Proj_03.png "Projection map")

![img](./imgs/Proj03_with_threshold.png "Projection map threshold at 90 percentile")



### `G-C2L1P-Feb23-B-Shelby_q2_04-06`

![img](./imgs/Proj_04.png "Projection map")

![img](./imgs/Proj04_with_threshold.png "Projection map threshold at 90 percentile")



## Inference
### Shortcomings

These projections have following shortcomings,

-   Produces large search area when groups are not settled
-   Hands that are always visible produce peaks even tho they are
    not performing typing or writing. This diminishes our chance of
    detecting regions that occasionally have hands, performing activity
    of interest.
-   The threshold is not learned properly



### Suggestions

To overcome thsese shortcomings

-   Do hand projections for short video clips
-   Instead of hard threshold (90 percentile) for marking a region as active, learn the
    thereshold using shallow random forest from training data.

