# Activity map: using typing and writing ground truth
**NOTE:** If you cannot access the youtube links please email venkatesh.jatla@gmail.com, CCing
  Prof. Pattichis for access.
<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
## Table of Contents

- [Activity map: using typing and writing ground truth](#activity-map-using-typing-and-writing-ground-truth)
    - [Download package](#download-package)
    - [Prerequisites](#prerequisites)
        - [Writing and typing ground truth](#writing-and-typing-ground-truth)
        - [Session properties file](#session-properties-file)
        - [Groups database as csv file](#groups-database-as-csv-file)
    - [Configuration file](#configuration-file)
    - [Python](#python)
    - [Uploading to AOLME website](#uploading-to-aolme-website)

<!-- markdown-toc end -->
## Download package
Please click the following link to download python script and all
the necessary files to plot activity map.  
<a href="https://vjatla.github.io/HAQ-gpages/how-to/activity_maps/activity_map_06_29_2021.zip">Download complete package (June 29th 2021)</a>
## Prerequisites
To create activity map from typing and writing we need the following
files,
1. Writing ground truth
2. Typing ground truth
3. Session properties file
4. Groups database as csv file

### Writing and typing ground truth
We provide writing and typing groud truth as excel. The excel should
have a sheet named *"Human readable"* containing activity instances.
A sample of writing ground truth is shown below along with a brief
description of each column.
<p align = "center">
	<img src = "./images/writing_gt_sample.PNG">
</p>
<p align = "center">
	Fig.1 Writing ground truth sample for C1L1P-E, Mar 02
</p>

| Column name  | Comment                                            |
|--------------|----------------------------------------------------|
| Video name   | Name of the video containing the activity instance |
.| Numeric code | Numeric code of person performing the activity     |
| Pseudonym    | Pseudonym of the person performing the activity    |
| Start time   | Activity start time in HH:MM:SS                    |
| End time     | Activity end time in HH:MM:SS                      |

### Session properties file
The session properties file has information about a section. We provide
a sample and brief description below.
<p align = "center">
	<img src = "./images/session_properties.PNG">
</p>
<p align = "center">
	Fig.2 Session properties for C1L1P-E, Mar 02
</p>

| Column name | Comment                                        |
|-------------|------------------------------------------------|
| width       | Video width                                    |
| height      | Video height                                   |
| dur         | Video duration in seconds                      |
| prev_dur    | Total duration before current video in seconds |
| name        | name of video file                             |

### Groups database as csv file
We use *"php my admin"* to export AOLME groups data base as CSV file.
For more information please refer the following 
[YouTube video](https://youtu.be/s874XxXjt38).
## Configuration file
We use a JSON file to pass all the necessary parameters to the python
script. A sample is presented below,
```json
{
    "Title":"Typing and writing activities in C1L1P-E Mar02",
    "Cohort":"1",
    "Level":"1",
    "School":"Polk",
    "Group":"E",
    "Date2":"2017-03-02",
    "Date":"Mar02",
    "Persons":["Kid51", "Kid15", "Kid25", "Kid17", "Kid10", "Kid16"],
    "Pseudonyms":["Jacinto", "Marina", "Emilio", "Jorge", "Herminio", "Juan"],
    "session_properties":"C:\\Users\\vj\\Dropbox\\AOLME_Activity_Maps\\GT\\C1L1P-E\\20170302\\properties_session.xlsx",
    "activities":"typing, writing",
    "activity_offset_val":[-0.15, 0.15],
    "activity_colors":["blue","green"],
    "typing_xlsx":"C:\\Users\\vj\\Dropbox\\typing-notyping\\C1L1P-E\\20170302\\typing_instances.xlsx",
    "writing_xlsx":"C:\\Users\\vj\\Dropbox\\writing-nowriting-GT\\C1L1P-E\\20170302\\writing_instances.xlsx",
    "groups_db":"C:\\Users\\vj\\Dropbox\\AOLME_Activity_Maps\\groups_jun28_2021.csv",
    "save_loc":"C:\\Users\\vj\\Dropbox\\AOLME_Activity_Maps\\GT\\C1L1P-E\\20170302\\C1L1P-E_Mar02.html"
}
```

## Python
To run the python script provided in the download package,
```bash
# Windows
python .\generate_activity_map.py .\all_activities_win.json
```
## Uploading to AOLME website
The HTML file saved on running the python script is,
1. Uploaded onto AOLME data server
2. An entry is added to AOLME data base with proper location.

Please check the following [YouTube
video](https://youtu.be/PjMF3dOGPQU) for further information.
