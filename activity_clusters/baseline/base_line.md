# Activity cluters Base line
<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Activity cluters Base line](#activity-cluters-base-line)
    - [Algorithm](#algorithm)
    - [Performance](#performance)
        - [Typing](#typing)
        - [Writing](#writing)
    - [More details](#more-details)
        - [Typing](#typing-1)
        - [Writing](#writing-1)

<!-- markdown-toc end -->

## Algorithm
1. Get meadian hand size by running hand detector for first 60

2. Cluster activities that are below two hand distances and update
the cluster centroid.
3. This algorithme is applied to the whole session.

## Performance

### Typing

| Session         | Clusters (stability, pred, gt) | Rand Index(RI) | # Activities | More details                      |
|-----------------|--------------------------------|----------------|--------------|-----------------------------------|
| C1L1p\_Apr13\_C | (unstable, 2, 4)               | 0.76           | 18           | [C1L1p\_Apr13\_C](#c1l1p_apr13_c) |
| C1L1P\_Mar02\_E | (stable,   6, 6)               | 0.97           | 45           | [C1L1P\_Mar02\_E](#c1l1p_mar02_e) |
| C1L1P\_Mar30\_C | (stable,   2, 2)               | 1.0            | 5            | [C1L1P\_Mar30\_C](#c1l1p_mar30_c) |
| C2L1P\_Feb23\_B | (unstable, 3, 5)               | 0.72           | 56           | [C2L1P\_Feb23\_B](#c2l1p_feb23_b) |
| C2L1P\_Mar08\_D | (unstable, 2, 4)               | 0.58           | 58           | [C2L1P\_Mar08\_D](#c2l1p_mar08_d) |
| C3L1P\_Apr11\_C | (stable,   2, 3)               | 0.89           | 13           | [C3L1P\_Apr11\_C](#c3l1p_apr11_c) |
| C3L1P\_Feb21\_D | (stable,   3, 4)               | 0.69           | 44           | [C3L1P\_Feb21\_D](#c3l1p_feb21_d) |


### Writing

| Session         | Clusters (stability, pred, gt) | Rand Index(RI)  | # Activities | More details                        |
|-----------------|--------------------------------|-----------------|--------------|-------------------------------------|
| C1L1P\_Apr13\_C | (stable, 3, 4)                 | 0.79            | 27           | [C1L1P\_Apr13\_C](#c1l1p_apr13_c)   |
| C1L1P\_Mar02\_E | (stable, 3, 3)                 | 1.0             | 60           | [C1L1P\_Mar02\_E](#c1l1p_mar02_e-1) |
| C2L1P\_Apr12\_E | *no hands data*                | *no hands data* | *no hands*   | [C2L1P\_Apr12\_E](#c2l1p_apr12_e)   |
| C2L1P\_Mar08\_D | (stable, 2, 2)                 | 0.56            | 14           | [C2L1P\_Mar08\_D](#c2l1p_mar08_d-1) |
| C3L1P\_Apr11\_C | (stable, 4, 4)                 | 0.95            | 109          | [C3L1P\_Apr11\_C](#c3l1p_apr11_c-1) |
| C3L1P\_Feb21\_D | (stable, 5, 5)                 | 0.95            | 23           | [C3L1P\_Feb21\_D](#c3l1p_feb21_d-1) |

## More details
### Typing
#### **C1L1p\_Apr13\_C**
![C1L1p\_Apr13\_C](./images/G-C1L1P-Apr13-C-Windy_q2_03-07_30fps.png)
+ RI: **0.76**
+ Videos having activities: **4** out of 7.
+ Keyboard type: **Wireless**
+ Failure analysis: 
  + **Error in ground truth**. There is no Mariana in the group!


#### **C1L1P\_Mar02\_E**
![C1L1p\_Mar02\_E](./images/G-C1L1P-Mar02-E-Irma_q2_04-08_30fps.png)
+ RI: **0.96**
+ Videos having activities: **7** out of 8.
+ Keyboard type: **Wireless**
+ Success analysis: 
  + Stable camera position
  + The kids did not exhibit global change in position.
  + The kdis took turns in typing.


#### **C1L1P\_Mar30\_C**
![C1L1p\_Mar30\_C](./images/G-C1L1P-Mar30-C-Kelly_q2_01-06.png)
+ RI: **1.0**
+ Videos having activities: **1** out of 8.
+ Keyboard type: **Wireless**
+ Success analysis: 
  + Very few instances of typing.
  
#### **C2L1P\_Feb23\_B**
![C1L1p\_Mar30\_C](./images/G-C2L1P-Feb23-B-Shelby_q2_05-06_30fps.png)
+ RI: **0.72**
+ Videos having activities: **5** out of 6.
+ Keyboard type: **Wired**
+ Failure analysis:
  + The keyboard is not passed around. Its position is fixed. The kids
  leaned towards it to type.
  + Multiple persons typing at the same time
  
| Video name                                   | Time stamp | Comment                |
|----------------------------------------------|------------|------------------------|
| G-C2L1P-Feb23-B-Shelby\_q2\_05-06\_30fps.png | 06:37      | Kids leaning           |
| G-C2L1P-Feb23-B-Shelby\_q2\_03-06\_30fps.png | 12:36      | Mutiple persons typing |

#### **C2L1P\_Mar08\_D**
![C2L1P\_Mar08\_D](./images/G-C2L1P-Mar08-D-Chaitu_q2_05-05_30fps.png)
+ RI: **0.58**
+ Videos having activities: **5** out of 5.
+ Keyboard type: **Wired**
+ Failure analysis:
  + The keyboard is not passed around.
  + Kids are standing around or leaning on to the keyboard to type for majority
  of the session.
  + The kids sat very close to each other, almost overlapping their activities.

#### **C3L1P\_Apr11\_C**
![C3L1P\_Apr11\_C](./images/G-C3L1P-Apr11-C-Phuong_q2_05-05_30fps.png)
+ RI: **0.89**
+ Videos having activities: **5** out of 5.
+ Keyboard type: **Wired**
+ Success analysis:
  + Few instances of typing.
  + Even tho the kids are sitting close to each other they are maintained
	proper distance during the activity.
  
#### **C3L1P\_Feb21\_D**
![C3L1P\_Feb21\_D](./images/G-C3L1P-Feb21-D-Ivonne_q2_05-05_30fps.png)
+ RI: **0.69**
+ Videos having activities: **5** out of 5.
+ Keyboard type: **Wired**
+ Failure analysis:
  + The keyboard is not moved around
  + Kids leaned towards the keyboard to type

### Writing
+ Reducing clustering distance threshold to 1 hand might be of help.

#### C1L1P\_Apr13\_C
![C1L1p\_Apr13\_C](./images/G-C1L1P-Apr13-C-Windy_q2_03-07_30fps.png)
+ RI: **0.797**

#### C1L1P\_Mar02\_E
![C1L1p\_Mar02\_E](./images/G-C1L1P-Mar02-E-Irma_q2_04-08_30fps.png)
+ RI: **1.0**
+ Analysis:
  + Very **well separated** writing instances.

#### C2L1P\_Apr12\_E
*Hands not available.*
  
#### C2L1P\_Mar08\_D
![C2L1P\_Mar08\_D](./images/G-C2L1P-Mar08-D-Chaitu_q2_05-05_30fps.png)
+ RI: **0.56**
+ Analysis:
  + Few instances of writing
  + The kids are standing around making acitivity association difficult.

#### C3L1P\_Apr11\_C
![C3L1P\_Apr11\_C](./images/G-C3L1P-Apr11-C-Phuong_q2_05-05_30fps.png)
+ RI: **0.95**
+ Analysis:
  + The writing instances are well separated.

#### C3L1P\_Feb21\_D
![C3L1P\_Feb21\_D](./images/G-C3L1P-Feb21-D-Ivonne_q2_05-05_30fps.png)
+ RI: **0.95**
+ Analysis:
  + The writing instances are well separated.
