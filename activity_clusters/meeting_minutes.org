* Sep 3, 2021
** Summary:
   Create a framework to manually label activity clusters
   (i) as quickly and
   (ii) as less human intervention as possible.
