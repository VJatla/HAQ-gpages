# SOTA Vs Dyadic 3D CNN on group leave one out
## Info
Performace of dyadic 3d cnn(winning=4 dyads) vs SOTA methods on group based leave one out dataset. The SOTA methods considered
for this experiment are,
1.  I3D
2.  slowfast
3.  slowonly
4.  TSN
5.  TSM *Unfortunately the TSM method trained, but errored during validation so we are not using it*.


## typing/notpying
### C1L1P-A
This group has a total of 72(33 ty, 39 nty) samples for validation.

| Method        | Best epoch | Trn Acc | Val Acc   | diff | Time |
|---------------|------------|---------|-----------|------|------|
| I3D           | 5          | 94.44   | **93.06** | 1.38 |      |
| slowfast      | 5          | 93.05   | 84.72     | 8.33 |      |
| slowonly      | 11         | 93.05   | 87.50     | 5.55 |      |
| TSN           | 12         | 91.66   | 81.90     | 9.76 |      |
| Dyadic 3D CNN | 15         | 91      | 92        | 1    |      |

### C1L1P-B
This group has a total of 95(55 ty, 40 nty) samples for validation.

| Method        | Best epoch | Trn Acc | Val Acc | diff | Time |
|---------------|------------|---------|---------|------|------|
| I3D           | 5          | 92.63   | 87.37   | 5.26 |      |
| slowfast      | 71         | 94.73   | 91.5    | 3.23 |      |
| slowonly      | 59         | 93.68   | 92.60   | 1.08 |      |
| TSN           | 24         | 92.63   | 86.32   | 6.31 |      |
| Dyadic 3D CNN | 34         | 93      | **94**  | 1    |      |

### C1L1P-C
This group has a total of 192(109 ty, 83 nty) samples for validation.

| Method        | Best epoch | Trn Acc | Val Acc   | diff | Time |
|---------------|------------|---------|-----------|------|------|
| I3D           | 40         | 91.66   | 87.50     | 4.16 |      |
| slowfast      | 128        | 92.18   | **91.15** | 1.03 |      |
| slowonly      | 148        | 86.45   | 84.38     | 2.07 |      |
| TSN           | 26         | 90.10   | 84.38     | 5.72 |      |
| Dyadic 3D CNN | 2          | 83      | 90        | 7    |      |

### C1L1W-A
This group has a total of 48 (30 ty, 18 nty) samples for validation.

| Method        | Best epoch | Trn Acc | Val Acc | diff | Time |
|---------------|------------|---------|---------|------|------|
| I3D           | 46         | 93.75   | 93.75   | 0    |      |
| slowfast      | 18         | 95.83   | 95.83   | 0    |      |
| slowonly      | 70         | 95.83   | 95.83   | 0    |      |
| TSN           | 19         | 93.75   | 95.83   | 2.08 |      |
| Dyadic 3D CNN | 29         | 93      | **98**  | 5    |      |

### C2L1W-B
This group has a total of 155(75 ty, 80 nty) samples for validation.

| Method        | Best epoch | Trn Acc | Val Acc   | diff  | Time |
|---------------|------------|---------|-----------|-------|------|
| I3D           | 2          | 84.51   | 83.87     | 0.64  |      |
| slowfast      | 3          | 87.74   | 83.23     | 4.51  |      |
| slowonly      | 8          | 87.4    | **85.81** | 1.59  |      |
| TSN           | 3          | 87.01   | 76.13     | 10.88 |      |
| Dyadic 3D CNN | 15         | 91      | 79        | 12    |      |

### C3L1W-D
This group has a total of 121(49 ty, 72 nty) samples for validation.

| Method        | Best epoch | Trn Acc | Val Acc | diff | Time |
|---------------|------------|---------|---------|------|------|
| I3D           | 6          | 90.90   | 91.74   | 0.84 |      |
| slowfast      | 8          | 89.25   | 88.43   | 0.82 |      |
| slowonly      | 55         | 90.90   | 90.08   | 0.82 |      |
| TSN           | 7          | 89.25   | 90.91   | 1.66 |      |
| Dyadic 3D CNN | 27         | 91      | **93**  | 3    |      |

## writing/nowriting
### C1L1P-B
This group has a total of 132(57 w,75 nw) samples for validation.

| Method        | Best epoch | Trn Acc | Val Acc | diff | Time |
|---------------|------------|---------|---------|------|------|
| I3D           | 13         | 82.57   | 80.30   | 2.27 |      |
| slowfast      | 59         | 86.36   | 81.82   | 4.54 |      |
| slowonly      | 36         | 80.30   | 76.52   | 3,78 |      |
| TSN           | 34         | 77.2    | 71.97   | 5.23 |      |
| Dyadic 3D CNN | 13         | 90      | 83      | 7    |      |

### C1L1P-C
This group has a total of 475(337 w,138 nw) samples for validation.

| Method        | Best epoch | Trn Acc | Val Acc | diff | Time |
|---------------|------------|---------|---------|------|------|
| I3D           | 6          | 89.47   | 88.63   | 0.84 |      |
| slowfast      | 71         | 90.73   | 89.68   | 1.1  |      |
| slowonly      | 107        | 88.84   | 90.11   | 1.27 |      |
| TSN           | 8          | 88.0    | 86.74   | 1.26 |      |
| Dyadic 3D CNN | 19         | 90      | 87      | 3    |      |

### C2L1P-B
This group has a total of 73(17 w,56 nw) samples for validation.

| Method        | Best epoch | Trn Acc | Val Acc | diff  | Time |
|---------------|------------|---------|---------|-------|------|
| I3D           | 11         | 79.45   | 72.60   | 6.85  |      |
| slowfast      | 26         | 82.19   | 72.60   | 9.59  |      |
| slowonly      | 116        | 79.45   | 68.49   | 10.96 |      |
| TSN           | 10         | 71.23   | 58.90   | 12.33 |      |
| Dyadic 3D CNN | 23         | 92      | 75      | 17    |      |

### C2L1P-C
This group has a total of 123(88 w,35 nw) samples for validation.

| Method        | Best epoch | Trn Acc | Val Acc | diff | Time |
|---------------|------------|---------|---------|------|------|
| I3D           | 21         | 91.05   | 91.87   | 0.82 |      |
| slowfast      | 133        | 93.4    | 91.87   | 1.62 |      |
| slowonly      | 15         | 89.4    | 84.55   | 4.88 |      |
| TSN           | 4          | 87.8    | 85.37   | 2.43 |      |
| Dyadic 3D CNN | 36         | 90      | 80      | 10   |      |
