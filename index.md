# [HAQ](https://vjatla.github.io/HAQ/)
## HOW-TO
### Activity maps
+ [Using typing and writing ground truth](./how-to/activity_maps/using_typing_writing_gt.md)  
  How to create activity maps using typing and writing ground truth.

### Activity clusters
+ [Baseline](./reports/activity_clusters/base_line.md)
  
